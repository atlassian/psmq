package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.psmq.api.message.QContentType;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageBuilder;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageId;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueDefinitionBuilder;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

public class TestApiMessageCrud extends BaseApiTest {

    @Test
    public void test_put_with_no_buffer() {
        // assemble
        String qName = Fixtures.rqName();
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition()
                .withName(qName)
                .build();

        // act
        Queue queue = qSession.queueOperations().accessQueue(queueDef);
        QMessageProducer messageProducer = qSession.createProducer(queue);

        // no buffer specified
        QMessage writtenMsg = QMessageBuilder.newMsg().withId(QMessageId.id("someId")).build();

        long writtenCount = messageProducer.writeMessage(writtenMsg);

        QMessageConsumer messageConsumer = qSession.createConsumer(queue);
        Optional<QMessage> readMsg = messageConsumer.claimAndResolve();

        // assert
        assertThat(writtenCount, equalTo(1L));
        assertThat(readMsg.isPresent(), equalTo(true));

        QMessage msg = readMsg.get();
        assertThat(msg.buffer().asString(), equalTo(""));
        assertThat(msg.messageId().value(), equalTo("someId"));
        assertThat(msg.contentType(), equalTo(QContentType.PLAIN_TEXT));
        assertThat(msg.buffer().length(), equalTo(0L));
        assertThat(msg.systemMetaData().createdDate().getTime() >= now, equalTo(true));
        assertThat(msg.expired(), equalTo(false));
        assertThat(msg.expiryDate().isPresent(), equalTo(false));
        assertThat(msg.messageId(), notNullValue());
    }
}
