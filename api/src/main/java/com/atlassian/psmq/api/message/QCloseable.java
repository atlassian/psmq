package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.QException;

/**
 * Things that can be closed should be closed, no system has infinite resources
 *
 * @since v1.0
 */
@PublicApi
public interface QCloseable extends AutoCloseable
{
    /**
     * @return true if the object is closed
     */
    boolean isClosed();

    @Override
    void close() throws QException;
}
