package com.atlassian.psmq.internal;

import com.atlassian.fugue.Option;
import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnectionConverter;
import com.atlassian.psmq.api.QConnectionProvider;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.QSessionDefinition;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.paging.PageResponse;
import com.atlassian.psmq.api.queue.QBrowseMessageQuery;
import com.atlassian.psmq.api.queue.QBrowseMessageQueryBuilder;
import com.atlassian.psmq.api.queue.QId;
import com.atlassian.psmq.api.queue.QTopic;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueBrowser;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueOperations;
import com.atlassian.psmq.api.queue.QueueQuery;
import com.atlassian.psmq.api.queue.QueueUpdate;
import com.atlassian.psmq.internal.io.MessageReader;
import com.atlassian.psmq.internal.io.MessageWriter;
import com.atlassian.psmq.internal.io.SessionInstructions;
import com.atlassian.psmq.internal.io.TxnBoundary;
import com.atlassian.psmq.internal.io.TxnBoundaryAutoCommit;
import com.atlassian.psmq.internal.io.TxnBoundarySession;
import com.atlassian.psmq.internal.io.TxnBoundarySessionExternal;
import com.atlassian.psmq.internal.io.TxnContext;
import com.atlassian.psmq.internal.io.TxnContextSetup;
import com.atlassian.psmq.internal.io.TxnContextSetupImpl;
import com.atlassian.psmq.internal.io.TxnFixture;
import com.atlassian.psmq.internal.io.db.CommitKit;
import com.atlassian.psmq.internal.message.QMessageConsumerImpl;
import com.atlassian.psmq.internal.message.QMessageProducerImpl;
import com.atlassian.psmq.internal.queue.QueueImpl;
import com.atlassian.psmq.internal.queue.QueueOperationsImpl;

import java.sql.Connection;
import java.util.Optional;
import java.util.function.Supplier;

import static com.atlassian.psmq.api.QSessionDefinition.CommitStrategy.AUTO_COMMIT;
import static com.atlassian.psmq.api.QSessionDefinition.CommitStrategy.EXTERNAL_COMMIT;
import static com.atlassian.psmq.api.QSessionDefinition.CommitStrategy.SESSION_COMMIT;
import static com.atlassian.psmq.api.internal.Validations.checkArgument;
import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.api.internal.Validations.checkState;
import static com.atlassian.psmq.internal.util.HelperKit.toOptional;

/**
 * This object is stateful in that it contains the session definition on how it should act.
 * <p>
 * Therefore its collaborators needs to take "instructions" about that state when the calls are made
 * </p>
 * <p>
 * This per stateful session-ness is the reason this is not a Component in IoC terms say.
 * </p>
 */
public class QSessionImpl extends QCloseableImpl implements QSession {
    private final DatabaseAccessor databaseAccessor;
    private final DatabaseConnectionConverter databaseConnectionConverter;
    private final Option<QConnectionProvider> connectionProvider;
    private final QSessionDefinition sessionDefinition;
    private final QueueOperationsImpl queueOperations;
    private final MessageWriter messageWriter;
    private final MessageReader messageReader;


    // use when its not session controlled committing
    private Option<Connection> currentConnection = Option.none();

    public QSessionImpl(DatabaseConnectionConverter databaseConnectionConverter, DatabaseAccessor databaseAccessor, final Option<QConnectionProvider> connectionProvider, final QSessionDefinition sessionDefinition, final QueueOperationsImpl queueOperations, final MessageWriter messageWriter, final MessageReader messageReader) {
        this.databaseConnectionConverter = databaseConnectionConverter;
        this.databaseAccessor = databaseAccessor;
        this.connectionProvider = connectionProvider;
        this.queueOperations = queueOperations;
        this.sessionDefinition = sessionDefinition;
        this.messageWriter = messageWriter;
        this.messageReader = messageReader;
    }

    @Override
    public QueueOperations queueOperations() {
        return new QueueOperations() {
            @Override
            public PageResponse<Queue> queryQueues(final QueueQuery query) {
                checkNotClosed();

                return queueOperations.queryQueues(instructions(), checkNotNull(query));
            }

            @Override
            public Queue getQueue(final QId queueId) throws QException {
                checkNotClosed();

                return queueOperations.getQueue(instructions(), queueId);
            }

            @Override
            public Queue updateQueue(final Queue queue, final QueueUpdate queueUpdate) throws QException {
                checkNotClosed();

                return queueOperations.updateQueue(instructions(), asQueueImpl(queue), queueUpdate);
            }

            @Override
            public Queue accessQueue(final QueueDefinition queueDefinition) {
                checkNotClosed();

                return queueOperations.accessQueue(instructions(), checkNotNull(queueDefinition));
            }

            @Override
            public Optional<Queue> exclusiveAccessQueue(final QueueDefinition queueDefinition) throws QException {
                checkNotClosed();

                return toOptional(queueOperations.exclusiveAccessQueue(instructions(), checkNotNull(queueDefinition)));
            }

            @Override
            public void heartBeatQueue(final Queue queue) throws QException {
                checkNotClosed();

                queueOperations.heartBeatQueue(instructions(), checkNotNull(queue));
            }

            @Override
            public void releaseQueue(final Queue queue) throws QException {
                checkNotClosed();

                queueOperations.releaseQueue(instructions(), checkNotNull(queue));
            }

            @Override
            public boolean releaseQueueIfEmpty(final Queue queue) throws QException {
                checkNotClosed();

                return queueOperations.releaseQueueIfEmpty(instructions(), checkNotNull(queue));
            }

            @Override
            public void releaseAllQueues() throws QException {
                checkNotClosed();

                queueOperations.releaseAllQueues(instructions());
            }

            @Override
            public void unresolveAllClaimedMessages(final Queue queue) {
                checkNotClosed();

                queueOperations.unresolveAllClaimedMessages(instructions(), asQueueImpl(queue));
            }


            @Override
            public void unresolveAllClaimedMessages() {
                checkNotClosed();

                queueOperations.unresolveAllClaimedMessages(instructions());
            }

            @Override
            public void purgeQueue(final Queue queue) {
                checkNotClosed();

                queueOperations.purgeQueue(instructions(), asQueueImpl(queue));
            }

            @Override
            public void deleteQueue(final Queue queue) {
                checkNotClosed();

                queueOperations.deleteQueue(instructions(), asQueueImpl(queue));
            }

            @Override
            public QueueBrowser browser(final Queue queue) {
                checkNotClosed();

                return new QueueBrowser() {
                    @Override
                    public long getMessageCount() {
                        return queueOperations.getMessageCount(instructions(), asQueueImpl(queue), QBrowseMessageQueryBuilder.anyMessage());
                    }

                    @Override
                    public long getMessageCount(final QBrowseMessageQuery query) {
                        return queueOperations.getMessageCount(instructions(), asQueueImpl(queue), query);
                    }

                    @Override
                    public PageResponse<QMessage> browse(final QBrowseMessageQuery query) {
                        return queueOperations.browse(instructions(), asQueueImpl(queue), query);
                    }
                };
            }
        };
    }

    @Override
    public QMessageConsumer createConsumer(final Queue queue) {
        checkNotClosed();

        return new QMessageConsumerImpl(messageReader,
                instructions(checkNotNull(queue)));
    }

    @Override
    public QMessageProducer createProducer(final Queue queue) {
        checkNotClosed();

        return new QMessageProducerImpl(
                messageWriter,
                instructions(checkNotNull(queue)));
    }

    @Override
    public QMessageProducer createProducer(final QTopic topic) throws QException {
        checkNotClosed();

        return new QMessageProducerImpl(
                messageWriter,
                instructions(checkNotNull(topic)));
    }

    @Override
    public void commit() throws QException {
        checkNotClosed();
        checkState(sessionDefinition.commitStrategy() == SESSION_COMMIT, "You must have defined this session as using session commit strategy to call this method");

        if (currentConnection.isDefined()) {
            synchronized (this) {
                Connection connection = currentConnection.get();
                try {
                    CommitKit.commit(connection, "Unable to commit QSession");
                } finally {
                    returnCurrentConnection(currentConnection);
                }
            }
        }
    }

    @Override
    public void rollback() throws QException {
        checkNotClosed();
        checkState(sessionDefinition.commitStrategy() == SESSION_COMMIT, "You must have defined this session as using session commit strategy to call this method");

        if (currentConnection.isDefined()) {
            synchronized (this) {
                Connection connection = currentConnection.get();
                try {
                    CommitKit.rollback(connection, "Unable to commit QSession");
                } finally {
                    returnCurrentConnection(currentConnection);
                }
            }
        }
    }

    @Override
    public void close() {
        synchronized (this) {
            super.close();
            returnCurrentConnection(currentConnection);
        }
    }

    private void returnCurrentConnection(final Option<Connection> connection) {
        if (currentConnection.isDefined()) {
            connectionProvider.get().returnConnection(connection.get());
            currentConnection = Option.none();
        }
    }

    private SessionInstructions instructions(final Queue queue) {
        checkNotNull(queue);

        QueueImpl q = asQueueImpl(queue);
        return new SessionInstructions(txnBoundary(sessionDefinition.commitStrategy()), q);
    }

    private SessionInstructions instructions(final QTopic topic) {
        checkNotNull(topic);

        return new SessionInstructions(txnBoundary(sessionDefinition.commitStrategy()), topic);
    }

    private SessionInstructions instructions() {
        return new SessionInstructions(txnBoundary(sessionDefinition.commitStrategy()));
    }

    private Supplier<TxnBoundary> txnBoundary(QSessionDefinition.CommitStrategy commitStrategy) {
        return () -> {
            // this will never commit and assume its done externally
            if (commitStrategy == EXTERNAL_COMMIT) {
                return new TxnBoundarySessionExternal(externalTxnFixture(), databaseConnectionConverter);
            } else if (commitStrategy == AUTO_COMMIT) {
                // this will commit on every message produced or consumed
                return new TxnBoundaryAutoCommit(autoCommitFixture(), databaseAccessor);
            } else {
                return new TxnBoundarySession(sessionLinkedTxnFixture(), databaseConnectionConverter);
            }
        };
    }

    private TxnFixture autoCommitFixture() {
        return new TxnFixture() {
            @Override
            public TxnContextSetup preTxn() {
                return new TxnContextSetupImpl(Option.none(), sessionDefinition.claimant(), sessionDefinition.claimantHeartBeatMillis());
            }

            @Override
            public void onTxnException(final TxnContext txnContext) {
            }

            @Override
            public void onTxnSuccess(final TxnContext txnContext) {
            }
        };
    }

    private TxnFixture sessionLinkedTxnFixture() {
        return new TxnFixture() {
            @Override
            public TxnContextSetup preTxn() {
                synchronized (this) {
                    checkState(connectionProvider.isDefined());
                    if (currentConnection.isEmpty()) {
                        currentConnection = Option.some(connectionProvider.get().borrowConnection());
                    }

                    return new TxnContextSetupImpl(currentConnection, sessionDefinition.claimant(), sessionDefinition.claimantHeartBeatMillis());
                }
            }

            @Override
            public void onTxnException(final TxnContext txnContext) {
            }

            @Override
            public void onTxnSuccess(final TxnContext txnContext) {
            }
        };
    }

    private TxnFixture externalTxnFixture() {
        return new TxnFixture() {
            @Override
            public TxnContextSetup preTxn() {
                checkState(connectionProvider.isDefined());
                Option<Connection> connection = Option.option(connectionProvider.get().borrowConnection());
                return new TxnContextSetupImpl(connection, sessionDefinition.claimant(), sessionDefinition.claimantHeartBeatMillis());
            }

            @Override
            public void onTxnException(final TxnContext txnContext) {
            }

            @Override
            public void onTxnSuccess(final TxnContext txnContext) {
            }
        };
    }

    @SuppressWarnings("ConstantConditions")
    private QueueImpl asQueueImpl(final Queue queue) {
        checkArgument(queue instanceof QueueImpl, "must be a QueueImpl");
        return (QueueImpl) queue;
    }

    private void checkNotClosed() {
        checkNotClosed("The session has been closed");
    }

}
