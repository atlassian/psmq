package com.atlassian.psmq.internal.queue;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.internal.Validations;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.paging.PageResponse;
import com.atlassian.psmq.api.queue.QBrowseMessageQuery;
import com.atlassian.psmq.api.queue.QId;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueQuery;
import com.atlassian.psmq.api.queue.QueueUpdate;
import com.atlassian.psmq.internal.io.SessionInstructions;
import com.atlassian.psmq.internal.io.TxnBoundary;
import com.atlassian.psmq.internal.io.db.QueueDao;
import com.atlassian.psmq.internal.io.db.QueueDbTO;
import com.google.common.base.Stopwatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.ParametersAreNonnullByDefault;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;
import static com.atlassian.psmq.api.internal.Validations.checkNotNull;
import static com.atlassian.psmq.internal.util.HelperKit.toOption;
import static java.lang.String.format;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

/**
 */
@Component
@ParametersAreNonnullByDefault
public class QueueOperationsImpl {

    private static final long FAIL_SAFE_MAX_TIME_MS = MILLISECONDS.convert(10, SECONDS);

    private final QueueDao queueDao;

    @Autowired
    public QueueOperationsImpl(final QueueDao queueDao) {
        this.queueDao = queueDao;
    }

    public Queue accessQueue(SessionInstructions instructions, QueueDefinition queueDefinition) throws QException {
        return accessQueueImpl(instructions, queueDefinition, QueueAccess.NONEXCLUSIVE).get();
    }

    public Option<Queue> exclusiveAccessQueue(final SessionInstructions instructions, final QueueDefinition queueDefinition) {
        return accessQueueImpl(instructions, queueDefinition, QueueAccess.EXCLUSIVE);
    }

    private Option<Queue> accessQueueImpl(final SessionInstructions instructions, final QueueDefinition queueDefinition, QueueAccess queueAccess) {
        checkNotNull(instructions);
        checkNotNull(queueDefinition);

        boolean queueAccessed = false;
        Option<Queue> queue = none();
        Stopwatch stopwatch = Stopwatch.createStarted();
        do {
            try {
                queue = accessOrCreateQueue(instructions.txnBoundary(), queueDefinition, queueAccess);
                queueAccessed = true;
            } catch (RuntimeException e) {
                // Ok let's assume this is was a create race miss.
                // Loop over and try accessing the queue again.
            }
        } while (!queueAccessed && stopwatch.elapsed(MILLISECONDS) < FAIL_SAFE_MAX_TIME_MS);

        //
        // check that the returned Q matches the desired definition
        queue.foreach(q -> Validations.checkQueueToItsDefinition(q, queueDefinition));
        return queue;
    }

    private Option<Queue> accessOrCreateQueue(TxnBoundary txn, QueueDefinition queueDefinition, QueueAccess queueAccess) {
        String queueName = queueDefinition.name();
        String generalErrorMsg = format("Unable to access queue '%s'", queueName);

        return txn.run(generalErrorMsg, txnContext -> {
            // this is a get or create operation
            QueueDbTO qDTO = new QueueDbTO(
                    queueName,
                    queueDefinition.purpose(),
                    toOption(queueDefinition.topic()),
                    queueDefinition.properties()
            );
            Option<QueueDbTO> queueDbTO = queueDao.selectQueue(txnContext, qDTO);
            if (queueDbTO.isEmpty()) {
                QueueDbTO queue = queueDao.createQueue(txnContext, qDTO);
                queueDbTO = some(queue);
            }
            if (queueAccess == QueueAccess.EXCLUSIVE) {
                long affected = queueDao.exclusiveAccess(txnContext, queueDbTO.get());
                if (affected == 0) {
                    return none();
                }
            }
            return queueDbTO.map(QueueDbTO::toQ);
        });
    }

    public void releaseQueue(final SessionInstructions instructions, final Queue queue) {
        checkNotNull(instructions);

        String generalErrorMsg = format("Unable to release queue '%s'", queue.name());

        TxnBoundary txn = instructions.txnBoundary();
        txn.run(generalErrorMsg, txnContext ->
        {
            // this is a get or create operation
            return queueDao.releaseQueue(txnContext, queue.id().value());
        });
    }

    public boolean releaseQueueIfEmpty(final SessionInstructions instructions, final Queue queue) {
        checkNotNull(instructions);

        String generalErrorMsg = format("Unable to release queue '%s'", queue.name());

        TxnBoundary txn = instructions.txnBoundary();
        return txn.run(generalErrorMsg, txnContext ->
                queueDao.releaseQueueIfEmpty(txnContext, queue.id().value())
        );
    }

    public void heartBeatQueue(final SessionInstructions instructions, final Queue queue) {
        checkNotNull(instructions);

        String generalErrorMsg = format("Unable to heart beat queue '%s'", queue.name());

        TxnBoundary txn = instructions.txnBoundary();
        txn.run(generalErrorMsg, txnContext ->
        {
            // this is a get or create operation
            return queueDao.heartBeatQueue(txnContext, queue.id().value());
        });
    }

    public void releaseAllQueues(final SessionInstructions instructions) {
        checkNotNull(instructions);

        String generalErrorMsg = "Unable to release all queues";

        TxnBoundary txn = instructions.txnBoundary();
        txn.run(generalErrorMsg, txnContext ->
        {
            // this is a get or create operation
            return queueDao.releaseAllQueues(txnContext);
        });
    }

    public void deleteQueue(SessionInstructions instructions, QueueImpl queue) throws QException {
        checkNotNull(instructions);
        checkNotNull(queue);

        long qId = queue.id().value();
        String generalErrorMsg = format("Unable to delete queue '%d'", qId);

        TxnBoundary txn = instructions.txnBoundary();
        txn.run(generalErrorMsg, connection -> queueDao.deleteQueue(connection, qId));
    }

    public void purgeQueue(final SessionInstructions instructions, final QueueImpl queue) {
        checkNotNull(instructions);
        checkNotNull(queue);

        long qId = queue.id().value();
        String generalErrorMsg = format("Unable to delete queue '%d'", qId);

        TxnBoundary txn = instructions.txnBoundary();
        txn.run(generalErrorMsg, txnContext -> queueDao.purgeQueue(txnContext, qId));
    }

    public PageResponse<QMessage> browse(SessionInstructions instructions, QueueImpl queue, final QBrowseMessageQuery query) {
        checkNotNull(instructions);
        checkNotNull(queue);

        String generalErrorMsg = format("Unable to browse queue '%s'", queue.name());

        TxnBoundary txn = instructions.txnBoundary();

        return txn.run(generalErrorMsg, txnContext ->
                queueDao.browse(txnContext, queue.id().value(), query).map(msgDbTO -> msgDbTO.toMessage(txnContext.claimant())));

    }

    public long getMessageCount(SessionInstructions instructions, final QueueImpl queue, final QBrowseMessageQuery query) {
        checkNotNull(instructions);
        checkNotNull(queue);

        String generalErrorMsg = format("Unable to browse queue count '%s'", queue.name());

        TxnBoundary txn = instructions.txnBoundary();
        return txn.run(generalErrorMsg, txnContext -> queueDao.getMessageCount(txnContext, queue.id().value(), query));
    }

    public PageResponse<Queue> queryQueues(final SessionInstructions instructions, final QueueQuery queueQuery) {
        checkNotNull(instructions);
        checkNotNull(queueQuery);

        String generalErrorMsg = "Unable to query queues";

        TxnBoundary txn = instructions.txnBoundary();
        return txn.run(generalErrorMsg, txnContext -> queueDao.queryQueues(txnContext, queueQuery).map(queueDbTO -> queueDbTO.toQ()));
    }

    public Queue getQueue(final SessionInstructions instructions, final QId queueId) {
        checkNotNull(instructions);

        long qId = queueId.value();
        String generalErrorMsg = format("Unable to find queue with id : %d", qId);

        TxnBoundary txn = instructions.txnBoundary();
        Option<Queue> queue = txn.run(generalErrorMsg, txnContext -> queueDao.getQueue(txnContext, qId).map(queueDbTO -> queueDbTO.toQ()));
        if (queue.isEmpty()) {
            throw new QException(generalErrorMsg);
        }
        return queue.get();
    }

    public Queue updateQueue(final SessionInstructions instructions, final QueueImpl queueImpl, final QueueUpdate queueUpdate) {
        checkNotNull(instructions);

        long queueId = queueImpl.id().value();
        String generalErrorMsg = format("Unable to update queue with id : %d ", queueId);

        TxnBoundary txn = instructions.txnBoundary();
        Option<Queue> queue = txn.run(generalErrorMsg, txnContext -> {
            long affected = queueDao.updateQueue(txnContext, queueId, queueUpdate);
            if (affected == 0) {
                throw new QException(generalErrorMsg);
            }
            return queueDao.getQueue(txnContext, queueId).map(queueDbTO -> queueDbTO.toQ());
        });
        if (queue.isEmpty()) {
            throw new QException(generalErrorMsg);
        }
        return queue.get();
    }

    public long unresolveAllClaimedMessages(final SessionInstructions instructions, QueueImpl queue) {
        checkNotNull(instructions);

        long queueId = queue.id().value();

        String generalErrorMsg = format("Unable to un-resolve in queue with id : %d ", queueId);

        TxnBoundary txn = instructions.txnBoundary();
        return txn.run(generalErrorMsg, txnContext -> queueDao.unresolveAllClaimedMessages(txnContext, queueId));
    }

    public long unresolveAllClaimedMessages(final SessionInstructions instructions) {
        checkNotNull(instructions);

        String generalErrorMsg = "Unable to un-resolve in session";

        TxnBoundary txn = instructions.txnBoundary();
        return txn.run(generalErrorMsg, txnContext -> queueDao.unresolveAllClaimedMessages(txnContext));
    }

    public enum QueueAccess {
        EXCLUSIVE, NONEXCLUSIVE
    }
}
