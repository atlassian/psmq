package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.queue.Queue;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 */
public class TestApiCommitExternalStrategies extends BaseApiTest {

    @Override
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    @Ignore("Need to come up with a better way to test concurrent updates")
    public void test_external_commit_happens() {
        psmqFixture.withConnection(connection -> {
            qSession = psmqFixture.externalCommitSession(connection);

            // assemble
            String qName = Fixtures.rqName();
            QSession otherSession = psmqFixture.autoCommitSession();

            Queue queueCommon = otherSession.queueOperations().accessQueue(Fixtures.qDefinition(qName));

            QMessageProducer producer1 = qSession.createProducer(queueCommon);
            producer1.writeMessage(Fixtures.basicMsg("Some message 1"));
            producer1.writeMessage(Fixtures.basicMsg("Some message 2"));

            // act
            QMessageConsumer consumerOther = qSession.createConsumer(queueCommon);

            // assert
            Optional<QMessage> otherMsg1 = consumerOther.claimAndResolve();
            Optional<QMessage> otherMsg2 = consumerOther.claimAndResolve();
            long otherSessionCount = otherSession.queueOperations().browser(queueCommon).getMessageCount();

            qSession.commit();

            Optional<QMessage> postCommitMsg1 = consumerOther.claimAndResolve();
            Optional<QMessage> postCommitMsg2 = consumerOther.claimAndResolve();

            // assert
            // the other session should not see work from the other session until its commits
            assertThat(otherMsg1.isPresent(), equalTo(false));
            assertThat(otherMsg2.isPresent(), equalTo(false));
            assertThat(otherSessionCount, equalTo(0L));

            assertThat(postCommitMsg1.isPresent(), equalTo(true));
            assertThat(postCommitMsg2.isPresent(), equalTo(true));

        });
    }
}
