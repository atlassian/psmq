package com.atlassian.psmq.backdoor.internal.util;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.psmq.api.QSessionFactory;
import com.atlassian.psmq.internal.visiblefortesting.QueueResetter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicReference;

/**
 * JUnit tests don't do IoC to this accessor gives us the ability to grab the QSessionFactory
 */
@Component
public class QComponentAccessor
{
    private static AtomicReference<QSessionFactory> qSessionFactoryRef = new AtomicReference<>();
    private static AtomicReference<QueueResetter> queueResetterRef = new AtomicReference<>();
    private static AtomicReference<DatabaseAccessor> databaseAccessorRef = new AtomicReference<>();

    @Autowired
    public QComponentAccessor(final QSessionFactory qSessionFactory, QueueResetter queueResetter, DatabaseAccessor databaseAccessor) {
        qSessionFactoryRef.set(qSessionFactory);
        queueResetterRef.set(queueResetter);
        databaseAccessorRef.set(databaseAccessor);
    }

    public static QSessionFactory getSessionFactory()
    {
        return getRef(qSessionFactoryRef);
    }

    public static QueueResetter getQueueResetter()
    {
        return getRef(queueResetterRef);
    }

    public static DatabaseAccessor getDatabaseAccessor()
    {
        return getRef(databaseAccessorRef);
    }

    private static <T> T getRef(AtomicReference<T> ref)
    {
        T t = ref.get();
        if (t == null)
        {
            throw new IllegalStateException("You have called too early!");
        }
        return t;
    }
}
