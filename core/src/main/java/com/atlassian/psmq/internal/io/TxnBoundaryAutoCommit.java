package com.atlassian.psmq.internal.io;

import com.atlassian.pocketknife.api.querydsl.DatabaseAccessor;
import com.atlassian.pocketknife.api.querydsl.DatabaseConnection;
import com.google.common.base.Function;

/**
 */
public class TxnBoundaryAutoCommit implements TxnBoundary {
    private final DatabaseAccessor databaseAccessor;
    private final TxnFixture txnFixture;

    public TxnBoundaryAutoCommit(final TxnFixture txnFixture, DatabaseAccessor databaseAccessor) {
        this.txnFixture = txnFixture;
        this.databaseAccessor = databaseAccessor;
    }

    @Override
    public <T> T run(final String operationMessage, final Function<TxnContext, T> txnFunction) {
        TxnContextSetup txnContextSetup = txnFixture.preTxn();

        return databaseAccessor.runInTransaction(dbConnection -> {
            TxnContext txnContext = makeTxnCtx(dbConnection, txnContextSetup);
            return txnFunction.apply(txnContext);
        });
    }

    private TxnContext makeTxnCtx(DatabaseConnection dbConnection, TxnContextSetup txnContextSetup) {
        return new TxnContextImpl(dbConnection, txnContextSetup.claimant(), txnContextSetup.claimantHeartBeatMillis());
    }
}
