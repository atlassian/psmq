package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.property.QProperty;
import com.atlassian.psmq.api.property.QPropertyBuilder;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;

/**
 * A builder of {@link QMessageUpdate}s
 *
 * @since v1.0
 */
@PublicApi
public class QMessageUpdateBuilder {
    private Set<QProperty> properties = new HashSet<>();
    private Optional<Date> expiryDate = Optional.empty();
    private Optional<QMessagePriority> priority = Optional.empty();

    public QMessageUpdateBuilder() {
    }

    /**
     * @return creates a new builder of {@link QMessageUpdate}s
     */
    public static QMessageUpdateBuilder newUpdate() {
        return new QMessageUpdateBuilder();
    }

    /**
     * Sets the expiry of the message.
     *
     * @param expiryDate the new expiry value
     *
     * @return this builder
     */
    public QMessageUpdateBuilder withExpiry(Date expiryDate) {
        this.expiryDate = Optional.of(checkNotNull(expiryDate));
        return this;
    }

    /**
     * Sets the priority of the message.
     *
     * @param priority the new priority value
     *
     * @return this builder
     */
    public QMessageUpdateBuilder withPriority(QMessagePriority priority) {
        this.priority = Optional.of(checkNotNull(priority));
        return this;
    }

    /**
     * Sets a string property value on the message.
     *
     * @param name  the new property name
     * @param value the new property value
     *
     * @return this builder
     */
    public QMessageUpdateBuilder withProperty(String name, String value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    /**
     * Sets a number property value on the message.
     *
     * @param name  the new property name
     * @param value the new property value
     *
     * @return this builder
     */
    public QMessageUpdateBuilder withProperty(String name, long value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    /**
     * Sets a boolean property value on the message.
     *
     * @param name  the new property name
     * @param value the new property value
     *
     * @return this builder
     */
    public QMessageUpdateBuilder withProperty(String name, boolean value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    /**
     * Sets a date property value on the message.
     *
     * @param name  the new property name
     * @param value the new property value
     *
     * @return this builder
     */
    public QMessageUpdateBuilder withProperty(String name, Date value) {
        this.properties.add(prop().with(name, value).build());
        return this;
    }

    /**
     * Sets a property value on the message.
     *
     * @param property the new property
     *
     * @return this builder
     */
    public QMessageUpdateBuilder withProperty(QProperty property) {
        this.properties.add(checkNotNull(property));
        return this;
    }

    /**
     * Sets a a set of property values on the message.
     *
     * @param properties the new properties
     *
     * @return this builder
     */
    public QMessageUpdateBuilder withProperties(Set<QProperty> properties) {
        for (QProperty property : checkNotNull(properties)) {
            withProperty(property);
        }
        return this;
    }

    private QPropertyBuilder prop() {
        return QPropertyBuilder.newProperty();
    }


    /**
     * @return the built {@link QMessageUpdate}
     */
    public QMessageUpdate build() {
        return new QMessageUpdate() {
            @Override
            public Optional<Date> expiryDate() {
                return expiryDate;
            }

            @Override
            public Optional<QMessagePriority> priority() {
                return priority;
            }

            @Override
            public Optional<Set<QProperty>> properties() {
                return properties.isEmpty() ? Optional.empty() : Optional.of(properties);
            }
        };
    }
}
