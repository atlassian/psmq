package com.atlassian.psmq.backdoor.internal.rest.resources;


import com.atlassian.invmtestrunner.api.server.JunitServerSideRemoteTestRunner;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.psmq.backdoor.internal.tests.TestApiCommitExternalStrategies;
import com.atlassian.psmq.backdoor.internal.tests.TestApiCommitSessionStrategies;
import com.atlassian.psmq.backdoor.internal.tests.TestApiMessageCrud;
import com.atlassian.psmq.backdoor.internal.tests.TestApiMessageOps;
import com.atlassian.psmq.backdoor.internal.tests.TestApiMessageQueries;
import com.atlassian.psmq.backdoor.internal.tests.TestApiQueueCrud;
import com.atlassian.psmq.backdoor.internal.tests.TestApiQueueOps;
import com.atlassian.psmq.backdoor.internal.tests.TestApiQueueQueries;
import com.atlassian.psmq.backdoor.internal.tests.TestApiTopicPublishing;
import com.atlassian.psmq.backdoor.internal.tests.TestMultipleQClaimants;
import com.atlassian.psmq.backdoor.internal.tests.TestReleaseIfEmptyMultipleWriters;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import static com.atlassian.invmtestrunner.api.server.TestRegistry.register;

/**
 * This REST resource can be hit to run a series of Junit tests inside the JVM
 */
@Produces(MediaType.APPLICATION_JSON)
@Path("tests")
@AnonymousAllowed
public class RemoteTestResource {
    static {
        register(TestApiCommitExternalStrategies.class);
        register(TestApiCommitSessionStrategies.class);
        register(TestApiMessageCrud.class);
        register(TestApiMessageOps.class);
        register(TestApiMessageQueries.class);
        register(TestApiQueueCrud.class);
        register(TestApiQueueOps.class);
        register(TestApiQueueQueries.class);
        register(TestApiTopicPublishing.class);
        register(TestMultipleQClaimants.class);
        register(TestReleaseIfEmptyMultipleWriters.class);
    }

    @GET
    public Response runTests(@Context UriInfo uriInfo) {
        return JunitServerSideRemoteTestRunner.runClasses(uriInfo);
    }
}
