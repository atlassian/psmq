package com.atlassian.psmq.internal.queue;

import com.atlassian.psmq.api.queue.QueueOperations;
import com.atlassian.psmq.internal.io.db.QueueDao;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 */
@Component
public class QueueOperationsFactory implements FactoryBean {
    private final QueueDao queueDao;

    @Autowired
    public QueueOperationsFactory(final QueueDao queueDao) {
        this.queueDao = queueDao;
    }

    @Override
    public Object getObject() throws Exception {
        return new QueueOperationsImpl(queueDao);
    }

    @Override
    public Class getObjectType() {
        return QueueOperations.class;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
