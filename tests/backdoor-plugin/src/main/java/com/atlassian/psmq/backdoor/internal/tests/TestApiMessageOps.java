package com.atlassian.psmq.backdoor.internal.tests;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageBuilder;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessagePriority;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.message.QMessageUpdateBuilder;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueDefinitionBuilder;
import org.junit.Test;

import java.util.Date;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

/**
 */
public class TestApiMessageOps extends BaseApiTest {
    @Test
    public void test_basic_message_properties() {
        // assemble
        Date nowDate = new Date(now);
        String qName = Fixtures.rqName();
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition()
                .withName(qName)
                .build();

        // act
        Queue queue = qSession.queueOperations().accessQueue(queueDef);
        QMessageProducer messageProducer = qSession.createProducer(queue);

        QMessage inputMsg = QMessageBuilder.newMsg()
                .newBuffer().append("propertyTest").buildBuffer()
                .withProperty("stringProperty", "someString")
                .withProperty("dateProperty", nowDate)
                .withProperty("longProperty", 666L)
                .withProperty("booleanProperty", false)
                .build();
        messageProducer.writeMessage(inputMsg);

        QMessageConsumer messageConsumer = qSession.createConsumer(queue);
        Optional<QMessage> readMsg = messageConsumer.claimAndResolve();

        // assert
        assertThat(readMsg.isPresent(), equalTo(true));

        QMessage msg = readMsg.get();
        assertThat(msg.properties().names().size(), equalTo(4));
        assertThat(msg.properties().stringValue("stringProperty").get(), equalTo("someString"));
        assertThat(msg.properties().dateValue("dateProperty").get(), equalTo(nowDate));
        assertThat(msg.properties().longValue("longProperty").get(), equalTo(666L));
        assertThat(msg.properties().booleanValue("booleanProperty").get(), equalTo(false));
    }


    @Test
    public void test_basic_message_update() {
        // assemble
        Date nowDate = new Date(now);
        Date originalExpiry = new Date(nowDate.getTime() + 100000);
        String qName = Fixtures.rqName();
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition()
                .withName(qName)
                .build();

        Queue queue = qSession.queueOperations().accessQueue(queueDef);
        QMessageProducer messageProducer = qSession.createProducer(queue);

        QMessage inputMsg = QMessageBuilder.newMsg()
                .newBuffer().append("propertyTest").buildBuffer()
                .withProperty("stringProperty", "someString")
                .withProperty("dateProperty", nowDate)
                .withProperty("longProperty", 666L)
                .withProperty("booleanProperty", false)
                .withExpiry(originalExpiry)
                .withPriority(QMessagePriority.LOW)
                .build();
        messageProducer.writeMessage(inputMsg);

        QMessageConsumer messageConsumer = qSession.createConsumer(queue);
        Optional<QMessage> readMsg = messageConsumer.claimMessage();
        QMessage msg = readMsg.get();

        assertThat(msg.expiryDate().get(), equalTo(originalExpiry));
        assertThat(msg.priority(), equalTo(QMessagePriority.LOW));

        // act

        Date newExpiry = new Date(originalExpiry.getTime() + 999);
        messageProducer.updateMessage(msg, QMessageUpdateBuilder.newUpdate()
                .withExpiry(newExpiry)
                .withPriority(QMessagePriority.HIGH)
                .withProperty("someOtherProperty", "someOtherString")
                .withProperty("someOtherNumber", 999L)
                .build()
        );
        // and put it back as readable
        messageConsumer.unresolveClaimedMessage(msg);


        // assert
        readMsg = messageConsumer.claimAndResolve();
        assertThat(readMsg.isPresent(), equalTo(true));

        msg = readMsg.get();
        assertThat(msg.properties().names().size(), equalTo(2));
        assertThat(msg.properties().stringValue("someOtherProperty").get(), equalTo("someOtherString"));
        assertThat(msg.properties().longValue("someOtherNumber").get(), equalTo(999L));
        assertThat(msg.expiryDate().get(), equalTo(newExpiry));
        assertThat(msg.priority(), equalTo(QMessagePriority.HIGH));
    }

    @Test
    public void test_message_claiming() throws Exception {
        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage in1 = Fixtures.buildMsg("m1").build();
        QMessage in2 = Fixtures.buildMsg("m2").build();
        QMessage in3 = Fixtures.buildMsg("m3").build();

        producer.writeMessage(in1);
        producer.writeMessage(in2);
        producer.writeMessage(in3);


        // act
        QMessageConsumer consumer = qSession.createConsumer(queue);

        Optional<QMessage> msg1 = consumer.claimMessage();
        Optional<QMessage> msg2 = consumer.claimMessage();
        Optional<QMessage> msg3 = consumer.claimMessage();
        // this should miss
        Optional<QMessage> msg4 = consumer.claimMessage();

        // now unread that third message
        consumer.unresolveClaimedMessage(msg3.get());

        // this should hit now since one was put back
        Optional<QMessage> msg5 = consumer.claimMessage();

        // assert
        assertThat(msg1.isPresent(), equalTo(true));
        assertThat(msg1.map(m -> m.buffer().asString()).get(), equalTo("m1"));

        assertThat(msg2.isPresent(), equalTo(true));
        assertThat(msg2.map(m -> m.buffer().asString()).get(), equalTo("m2"));

        assertThat(msg3.isPresent(), equalTo(true));
        assertThat(msg3.map(m -> m.buffer().asString()).get(), equalTo("m3"));

        assertThat(msg4.isPresent(), equalTo(false));

        assertThat(msg5.isPresent(), equalTo(true));
        assertThat(msg5.map(m -> m.buffer().asString()).get(), equalTo("m3"));
    }


    @Test
    public void test_message_claiming_with_resolve() throws Exception {
        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage in1 = Fixtures.buildMsg("m1").build();
        QMessage in2 = Fixtures.buildMsg("m2").build();
        QMessage in3 = Fixtures.buildMsg("m3").build();

        producer.writeMessage(in1);
        producer.writeMessage(in2);
        producer.writeMessage(in3);

        // act
        QMessageConsumer consumer = qSession.createConsumer(queue);

        Optional<QMessage> msg1 = consumer.claimMessage();
        Optional<QMessage> msg2 = consumer.claimMessage();

        // put m1 back but then resolve m2
        consumer.unresolveClaimedMessage(msg1.get());
        consumer.resolveClaimedMessage(msg2.get());

        Optional<QMessage> msg3 = consumer.claimMessage();
        Optional<QMessage> msg4 = consumer.claimMessage();
        Optional<QMessage> msg5 = consumer.claimMessage();

        // assert
        assertThat(msg1.isPresent(), equalTo(true));
        assertThat(msg1.map(m -> m.buffer().asString()).get(), equalTo("m1"));

        assertThat(msg2.isPresent(), equalTo(true));
        assertThat(msg2.map(m -> m.buffer().asString()).get(), equalTo("m2"));

        // because we put m1 back our 3rd read is back to m1
        assertThat(msg3.isPresent(), equalTo(true));
        assertThat(msg3.map(m -> m.buffer().asString()).get(), equalTo("m1"));

        // but m2 was resolved so next is m3
        assertThat(msg4.isPresent(), equalTo(true));
        assertThat(msg4.map(m -> m.buffer().asString()).get(), equalTo("m3"));

        // and there was no more after that
        assertThat(msg5.isPresent(), equalTo(false));
    }

    @Test(expected = QException.class)
    public void test_message_cross_claiming_contamination() throws Exception {
        QSession session1 = qSession;
        QSession session2 = psmqFixture.autoCommitSession();

        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage in1 = Fixtures.buildMsg("m1").build();
        QMessage in2 = Fixtures.buildMsg("m2").build();
        QMessage in3 = Fixtures.buildMsg("m3").build();

        producer.writeMessage(in1);
        producer.writeMessage(in2);
        producer.writeMessage(in3);

        // act
        QMessageConsumer consumer = session1.createConsumer(queue);
        QMessageConsumer otherConsumer = session2.createConsumer(queue);

        Optional<QMessage> msg1 = otherConsumer.claimMessage();

        // m1 was not read by session 1
        consumer.unresolveClaimedMessage(msg1.get());
    }

    @Test
    public void test_claiming_count() throws Exception {
        Queue queue = Fixtures.createQ(qSession, Option.some(Fixtures.rqName()), Option.none());

        QMessageProducer producer = qSession.createProducer(queue);

        QMessage in1 = Fixtures.buildMsg("m1").build();
        producer.writeMessage(in1);

        // act
        QMessageConsumer consumer = qSession.createConsumer(queue);
        for (int i = 0; i < 5; i++) {
            QMessage msg = consumer.claimMessage().get();
            consumer.unresolveClaimedMessage(msg);
        }

        QMessage msg = consumer.claimAndResolve().get();

        assertThat(msg.systemMetaData().claimCount(), equalTo(5));

    }

    @Test
    public void test_message_expiration() throws Exception {
        QueueDefinition queueDef = QueueDefinitionBuilder.newDefinition().withName(Fixtures.rqName()).build();

        QSession session = psmqFixture.autoCommitSession();

        Queue q = session.queueOperations().exclusiveAccessQueue(queueDef).get();

        QMessageProducer producer = session.createProducer(q);
        QMessage in = Fixtures.buildMsg("test").withExpiryFromNow(0).build();
        producer.writeMessage(in);

        QMessageConsumer consumer = session.createConsumer(q);
        Optional<QMessage> out = consumer.claimAndResolve();

        assertFalse(out.isPresent());
    }
}