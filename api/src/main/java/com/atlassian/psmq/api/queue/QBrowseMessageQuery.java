package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.message.QMessageQuery;
import com.atlassian.psmq.api.paging.LimitedPageRequest;

/**
 * A browse message query is used to find specific types of messages via the {@link
 * com.atlassian.psmq.api.queue.QueueBrowser} interface
 *
 * @since v1.0
 */
@PublicApi
public interface QBrowseMessageQuery extends QMessageQuery
{
    /**
     * @return the paging to use
     */
    LimitedPageRequest paging();

}
