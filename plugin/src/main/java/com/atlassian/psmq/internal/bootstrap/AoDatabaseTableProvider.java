package com.atlassian.psmq.internal.bootstrap;

import com.atlassian.psmq.spi.QDatabaseTableProvider;
import org.springframework.stereotype.Component;

/**
 * The AO implementation of QDatabaseTableProvider
 */
@Component
public class AoDatabaseTableProvider implements QDatabaseTableProvider {

    private static final String AO = "AO_319474_";  // <-- com.atlassian.psmq namespace

    @Override
    public String getTableName(LogicalTableName table) {
        switch (table) {
            case QUEUE:
                return AO + "QUEUE";
            case MESSAGE:
                return AO + "MESSAGE";
            case MESSAGE_PROPERTY:
                return AO + "MESSAGE_PROPERTY";
            case QUEUE_PROPERTY:
                return AO + "QUEUE_PROPERTY";
        }
        return null;
    }
}
