package com.atlassian.psmq.api.paging;

import com.atlassian.annotations.PublicApi;
import com.google.common.base.Function;

import java.util.List;
import java.util.Optional;

/**
 * A response to a page request, used in pagination.  Wraps a list and provides information on whether there are more
 * results.
 */
@PublicApi
public interface PageResponse<T> extends Iterable<T>
{
    /**
     * @return the results
     */
    List<T> getResults();

    /**
     * @return the number of results in this page of results
     */
    int size();

    /**
     * Indicates whether the source has more results after this page.
     *
     * @return true if there are more results
     */
    boolean hasMore();

    /**
     * @return the request used to produce this response
     */
    PageRequest getPageRequest();


    /**
     * Allows you to map a paged response of T into M
     * @param mapper the mapping function
     * @param <M> the output type
     * @return a paged response of M
     */
    <M> PageResponse<M> map(Function<T,M> mapper);

    /**
     * @return an optional first element of the paged response
     */
    Optional<T> findFirst();
}
