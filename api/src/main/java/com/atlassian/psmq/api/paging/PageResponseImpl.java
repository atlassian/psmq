package com.atlassian.psmq.api.paging;

import com.atlassian.annotations.PublicApi;
import com.google.common.base.Function;
import com.google.common.base.Objects;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * A wrapper around a list of content.
 */
@PublicApi
public class PageResponseImpl<T> implements PageResponse<T>
{
    // the request used to build this response
    private final PageRequest pageRequest;

    private ImmutableList<T> wrappedList;

    private final boolean hasMore;

    private PageResponseImpl(Builder<T> builder)
    {
        this.wrappedList = builder.listBuilder.build();
        this.hasMore = builder.hasMore;
        this.pageRequest = builder.request;
    }

    @Override
    public PageRequest getPageRequest()
    {
        return pageRequest;
    }

    @Override
    public Iterator<T> iterator()
    {
        return wrappedList.iterator();
    }

    @Override
    public int size()
    {
        return wrappedList.size();
    }

    @Override
    public List<T> getResults()
    {
        return wrappedList;
    }

    @Override
    public boolean hasMore()
    {
        return hasMore;
    }

    @Override
    public <M> PageResponse<M> map(final Function<T, M> mapper)
    {
        return transform(this,mapper);
    }

    @Override
    public Optional<T> findFirst()
    {
        return this.getResults().stream().findFirst();
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this).add("request", pageRequest).add("hasMore", hasMore).add("list", wrappedList).toString();
    }

    public static <T> Builder<T> from(Iterable<T> list, boolean hasMore)
    {
        Builder<T> builder = new Builder<>();
        builder.addAll(list);
        builder.hasMore = hasMore;
        return builder;
    }

    public static <T> Builder<T> fromSingle(T element, boolean hasMore)
    {
        if (element != null)
        {
            return from(Lists.newArrayList(element), hasMore);
        }

        return from(new ArrayList<>(), hasMore);
    }

    /**
     * Converts a given PageResponse contains items of type F into an equivalent PageResponse with items of type T,
     * transformed by the given mapper. All other attributes of PageResponse are kept intact.
     */
    public static <F, T> PageResponseImpl<T> transform(PageResponse<F> input, Function<F, T> mapper)
    {
        Iterable<T> transformedItems = Iterables.transform(input.getResults(), mapper);
        return from(transformedItems, input.hasMore()).pageRequest(input.getPageRequest()).build();
    }

    public static <T> PageResponseImpl<T> empty(boolean hasMore)
    {
        return from(new ArrayList<T>(), hasMore).build();
    }

    public static <T> PageResponseImpl<T> empty(boolean hasMore, PageRequest request)
    {
        return from(new ArrayList<T>(), hasMore).pageRequest(request).build();
    }

    public static <T> PageResponseImpl<T> empty(boolean hasMore, LimitedPageRequest request)
    {
        return from(new ArrayList<T>(), hasMore).pageRequest(request).build();
    }

    /**
     * Reduces a list of items for a limited request by the predicate provided. Useful for permission checks that are
     * known to the Manager layer...
     */
    public static <T> PageResponse<T> filteredPageResponse(LimitedPageRequest limitedPageRequest, List<T> items, Predicate<? super T> predicate)
    {
        //We really should not return more FILTERED elements than this.
        //Also we say that "hasMore==true" if there are more elements than this.
        final boolean hasMore = items.size() > limitedPageRequest.getLimit();

        if (predicate == null)
        {
            predicate = Predicates.alwaysTrue();
        }

        //preparing empty container
        final List<T> filteredItems = Lists.newArrayList();

        //removing extra elements returned by DAO _BEFORE_ we do filtering
        if (items.size() > limitedPageRequest.getLimit())
        {
            items = items.subList(0, limitedPageRequest.getLimit());
        }

        //now collecting our response
        for (T item : items)
        {
            if (predicate.apply(item))
            {
                filteredItems.add(item);
            }
        }

        return PageResponseImpl.from(filteredItems, hasMore).pageRequest(limitedPageRequest).build();
    }

    public static class Builder<T>
    {
        private boolean hasMore;
        private ImmutableList.Builder<T> listBuilder = ImmutableList.builder();
        private PageRequest request;

        public PageResponseImpl<T> build()
        {
            return new PageResponseImpl<>(this);
        }

        public Builder<T> add(T add)
        {
            listBuilder.add(add);
            return this;
        }

        public Builder<T> addAll(Iterable<T> toAdd)
        {
            listBuilder.addAll(toAdd);
            return this;
        }

        public Builder<T> pageRequest(PageRequest request)
        {
            this.request = request;
            return this;
        }

        public Builder<T> pageRequest(LimitedPageRequest limitedPageRequest)
        {
            this.request = new SimplePageRequest(limitedPageRequest);
            return this;
        }
    }
}
