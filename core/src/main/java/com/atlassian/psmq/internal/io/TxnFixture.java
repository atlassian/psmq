package com.atlassian.psmq.internal.io;

/**
 * This represents what should happen before an Txn is begun and what should happen
 * should it success or fail or even return successful streamy results
 */
public interface TxnFixture
{
    /**
     * The fixture is responsible for giving off a Txn context for inner called code
     *
     * @return a TxnContext to use for called code
     */
    TxnContextSetup preTxn();

    /**
     * This will be called if the Txn fails with an exception and hence needs to cleanup resources like the connection
     *
     * @param txnContext the TxnContext in play
     */
    void onTxnException(TxnContext txnContext);

    /**
     * This will be called if the Txn succeeds and hence needs to cleanup resources like the connection
     *
     * @param txnContext the TxnContext in play
     */
    void onTxnSuccess(TxnContext txnContext);
}
