package com.atlassian.psmq.internal.bootstrap;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.psmq.internal.util.logging.LogLeveller;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import static org.slf4j.LoggerFactory.getLogger;

/**
 * The Launcher is the starting point for the plugin.
 *
 * This should be the only life cycle listener in the system and it should then delegate out to other code as
 * appropriate.
 *
 * Its purpose is to allow us to reason about where events enter the system and the order of called code.  Having a
 * single listener / entry point object allows us to do that.
 */
@ExportAsService
@Component
public class Launcher implements LifecycleAware {
    private static final Logger log = getLogger(Launcher.class);

    private final EventPublisher eventPublisher;

    @Autowired
    public Launcher(final EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
        //
        // we want our logs to go out as info even if the host defaults to warn
        LogLeveller.setInfo(log);
    }

    @Override
    public void onStop() {
    }

    @EventListener
    public final void onClearCache(final Object event) {
        //
        // Since we don't compile against JIRA we don't have a type safe class name to link against
        // hence the use of a direct string.   This is really important for testing and support
        //
        if (event.getClass().getName().equals("com.atlassian.jira.event.ClearCacheEvent")) {
            // until we have been fully started at least once, we don't respond to clear cache
            // this handles a case during initial setup of JIRA
                onJiraClearCache();
        }
    }

    @PostConstruct
    public void onSpringContextStarted() {
        log.info("PSMQ spring context is starting...");
    }

    @Override
    public void onStart() {

        log.info("PSMQ REST is initializing...");
        eventPublisher.register(this);
        log.info("PSMQ REST is initialized.");
    }

    /**
     * This is called during functional tests in JIRA to clear the state of the plugin for testing purposes.  Its a soft
     * reset of sorts.
     */
    private void onJiraClearCache() {
        log.info("PSMQ REST is clearing it caches...");

        log.info("PSMQ REST has cleared it caches.");
    }

    @PreDestroy
    public void onSpringContextStopped() {
        log.info("PSMQ REST spring context is stopping...");

        eventPublisher.unregister(this);

        log.info("PSMQ REST spring context is stopped.");
    }
}
