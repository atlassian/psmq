package com.atlassian.psmq.internal.io.db;

import com.atlassian.psmq.api.QException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 */
public class CommitKit
{
    /**
     * Rollbacks the connection and then throws a QException
     *  @param connection the connection to rollback
     * @param operationMsg the error message*/
    public static void rollback(Connection connection, String operationMsg) throws QException
    {
        try
        {
            connection.rollback();
        }
        catch (SQLException e)
        {
            throw new QException(operationMsg);
        }
    }

    /**
     * Commits the connection and if that fails then throws a QException
     *  @param connection the connection to commit
     * @param exceptionMessage the error message*/
    public static void commit(Connection connection, String exceptionMessage) throws QException
    {
        try
        {
            connection.commit();
        }
        catch (SQLException e)
        {
            throw new QException(exceptionMessage);
        }
    }

}
