package com.atlassian.psmq.api.property;

import com.atlassian.annotations.PublicApi;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * A builder of {@link com.atlassian.psmq.api.property.QPropertySet}s.
 *
 * @since v1.0
 */
@PublicApi
public class QPropertySetBuilder
{
    QPropertyBuilder propertyBuilder = new QPropertyBuilder();
    Set<QProperty> properties = new HashSet<>();

    public static QPropertySetBuilder newSet()
    {
        return new QPropertySetBuilder();
    }

    public static QPropertySet newPropertySet(Set<QProperty> properties)
    {
        return QPropertyBuilder.newPropertySet(properties);
    }

    public QPropertySetBuilder()
    {
    }

    public QPropertySetBuilder with(String name, String value)
    {
        this.properties.add(propertyBuilder.with(name, value).build());
        return this;
    }

    public QPropertySetBuilder with(String name, long value)
    {
        this.properties.add(propertyBuilder.with(name, value).build());
        return this;
    }

    public QPropertySetBuilder with(String name, boolean value)
    {
        this.properties.add(propertyBuilder.with(name, value).build());
        return this;
    }

    public QPropertySetBuilder with(String name, Date value)
    {
        this.properties.add(propertyBuilder.with(name, value).build());
        return this;
    }

    public QPropertySet build()
    {
        return QPropertyBuilder.newPropertySet(this.properties);
    }
}
