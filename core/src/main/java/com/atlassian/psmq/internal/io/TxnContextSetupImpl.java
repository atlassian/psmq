package com.atlassian.psmq.internal.io;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.message.QClaimant;

import java.sql.Connection;

public class TxnContextSetupImpl implements TxnContextSetup {

    final QClaimant claimant;
    final Option<Connection> connection;
    final long claimantHeartBeat;

    public TxnContextSetupImpl(Option<Connection> connection, QClaimant claimant, long claimantHeartBeat) {
        this.claimant = claimant;
        this.connection = connection;
        this.claimantHeartBeat = claimantHeartBeat;
    }

    @Override
    public Option<Connection> connection() {
        return connection;
    }

    @Override
    public QClaimant claimant() {
        return claimant;
    }

    @Override
    public long claimantHeartBeatMillis() {
        return claimantHeartBeat;
    }
}
