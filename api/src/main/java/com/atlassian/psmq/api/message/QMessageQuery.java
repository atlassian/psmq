package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.property.query.QPropertyQuery;

import java.util.Optional;

/**
 * A message query is used to find specific types of messages
 *
 * @since v1.0
 */
@PublicApi
public interface QMessageQuery {
    /**
     * @return (optionally) the content type to filter messages by
     */
    Optional<QContentType> contentType();

    /**
     * @return (optionally) the content version to filter messages by
     */
    Optional<QContentVersion> contentVersion();

    /**
     * @return (optionally) the message priority to filter messages by
     */
    Optional<QMessagePriority> priority();

    /**
     * @return (optionally) the message id to filter messages by
     */
    Optional<QMessageId> messageId();

    /**
     * @return the property query to filter messages by
     */
    Optional<QPropertyQuery> properties();


}
