package com.atlassian.psmq.internal.rest.representations;

import com.atlassian.psmq.api.property.QProperty;
import com.atlassian.psmq.api.property.QPropertySet;
import com.google.common.collect.Collections2;

import java.util.Collection;

/**
 */
public class PropertyDTO {
    private final String name;
    private final String value;
    private final String type;

    public PropertyDTO(QProperty qProperty) {
        this.name = qProperty.name();
        this.value = qProperty.asString();
        this.type = qProperty.type().toString();
    }

    public static Collection<PropertyDTO> of(QPropertySet qPropertySet) {
        return Collections2.transform(qPropertySet.asSet(), input -> new PropertyDTO(input));
    }
}
