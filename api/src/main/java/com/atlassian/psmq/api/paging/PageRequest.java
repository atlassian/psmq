package com.atlassian.psmq.api.paging;

import com.atlassian.annotations.PublicApi;

/**
 * Specifies the pagination requirements of a request.
 */
@PublicApi
public interface PageRequest
{
    /**
     * @return start index for the page of results
     */
    int getStart();

    /**
     * @return the number of the results to return in the page
     */
    int getLimit();
}