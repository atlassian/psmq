package com.atlassian.psmq.internal.rest.implementation;

import com.atlassian.fugue.Option;
import com.atlassian.psmq.api.QException;
import com.atlassian.psmq.api.QSession;
import com.atlassian.psmq.api.QSessionDefinition;
import com.atlassian.psmq.api.QSessionDefinitionBuilder;
import com.atlassian.psmq.api.QSessionFactory;
import com.atlassian.psmq.api.message.QContentType;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.message.QMessageBuilder;
import com.atlassian.psmq.api.message.QMessageConsumer;
import com.atlassian.psmq.api.message.QMessageProducer;
import com.atlassian.psmq.api.paging.PageRequest;
import com.atlassian.psmq.api.paging.PageResponse;
import com.atlassian.psmq.api.queue.QBrowseMessageQuery;
import com.atlassian.psmq.api.queue.QBrowseMessageQueryBuilder;
import com.atlassian.psmq.api.queue.QId;
import com.atlassian.psmq.api.queue.Queue;
import com.atlassian.psmq.api.queue.QueueDefinition;
import com.atlassian.psmq.api.queue.QueueDefinitionBuilder;
import com.atlassian.psmq.api.queue.QueueQuery;
import com.atlassian.psmq.api.queue.QueueQueryBuilder;
import com.atlassian.psmq.internal.rest.representations.MessageDTO;
import com.atlassian.psmq.internal.rest.representations.MessageNoBufferDTO;
import com.atlassian.psmq.internal.rest.representations.PutMessageInstruction;
import com.atlassian.psmq.internal.rest.representations.QueueDTO;
import com.atlassian.psmq.internal.util.rest.JsonOut;
import com.atlassian.psmq.internal.util.rest.RestKit;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;

import static com.atlassian.fugue.Option.option;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

/**
 */
@Component
public class QueuesRestController {
    public static final int DEFAULT_RETURNED = 100;
    private final QSessionFactory qSessionFactory;

    @Autowired
    public QueuesRestController(final QSessionFactory qSessionFactory) {
        this.qSessionFactory = qSessionFactory;
    }

    public Response api(final UriInfo uriInfo) {
        StreamingOutput stream = JsonOut.run(jsonOut -> {
            LinksWriter links = new LinksWriter(uriInfo, jsonOut);
            links.buildApiLinks();
        });
        return Response.ok(stream).build();
    }

    public Response getAllQueues(final UriInfo uriInfo) {
        PageRequest paging = RestPageRequest.getPaging(uriInfo, DEFAULT_RETURNED);
        QueueQuery query = QueueQueryBuilder.newQuery().withPaging(paging).build();
        StreamingOutput out = JsonOut.run(jsonOut -> {
            LinksWriter links = new LinksWriter(uriInfo, jsonOut);

            links.buildApiLinks();

            streamQs(links, jsonOut, query);
        });
        return Response.ok(out).build();
    }


    public Response getQueueById(final UriInfo uriInfo, final long queueId) {
        QueueQuery query = QueueQueryBuilder.newQuery().withId(QId.id(queueId)).build();
        StreamingOutput out = JsonOut.run(jsonOut -> {
            LinksWriter links = new LinksWriter(uriInfo, jsonOut);

            links.buildApiLinks();
            streamQ(links, jsonOut, query);
        });
        return Response.ok(out).build();
    }

    public Response getQueueByName(final UriInfo uriInfo, final String queueName) {
        checkNotNull(queueName);

        QueueQuery query = QueueQueryBuilder.newQuery().withName(queueName).build();
        StreamingOutput out = JsonOut.run(jsonOut -> {

            LinksWriter links = new LinksWriter(uriInfo, jsonOut);

            links.buildApiLinks();
            streamQ(links, jsonOut, query);
        });
        return Response.ok(out).build();
    }

    public Response createQueue(final UriInfo uriInfo, final String name, final String purpose) {
        StreamingOutput out = JsonOut.run(jsonOut -> {
            QueueDefinition def = QueueDefinitionBuilder.newDefinition().withName(name).withPurpose(purpose).build();
            Queue q = session().queueOperations().accessQueue(def);

            LinksWriter links = new LinksWriter(uriInfo, jsonOut);

            links.buildApiLinks();

            jsonOut.propertyNamed("queues", () -> {
                links.buildQueueLinks(q);

                jsonOut.name("queue");
                jsonOut.toJson(new QueueDTO(q));
            });
        });
        return Response.ok(out).build();
    }

    private void streamQ(final LinksWriter links, final JsonOut jsonOut, final QueueQuery queueQuery) {
        streamQsImpl(links, jsonOut, queueQuery);
    }

    private void streamQs(final LinksWriter links, final JsonOut jsonOut, final QueueQuery queueQuery) {
        streamQsImpl(links, jsonOut, queueQuery);
    }

    private void streamQsImpl(final LinksWriter links, final JsonOut jsonOut, final QueueQuery queueQuery) {
        PageResponse<Queue> queues = session().queueOperations().queryQueues(queueQuery);
        jsonOut.arrayNamed("queues", () -> queues.forEach(q -> jsonOut.inObject(() -> streamQWithLinks(links, jsonOut, q))));
    }

    private void streamQWithLinks(final LinksWriter links, final JsonOut jsonOut, final Queue q) {
        jsonOut.propertyNamed("queue", () -> {
            links.buildQueueLinks(q);
            jsonOut.toJson("details", new QueueDTO(q));
        });
    }

    public Response browseQueueMessagesById(final UriInfo uriInfo, final HttpHeaders httpHeaders, final long queueId)
            throws QException {
        QSession session = session();
        Queue queue = session.queueOperations().getQueue(QId.id(queueId));
        if (notModified(queue, httpHeaders)) {
            return Response.notModified().build();
        }

        PageRequest paging = RestPageRequest.getPaging(uriInfo, DEFAULT_RETURNED);
        QBrowseMessageQuery query = QBrowseMessageQueryBuilder.newQuery().withPaging(paging).build();

        StreamingOutput streamOut = JsonOut.run(jsonOut -> {

            LinksWriter links = new LinksWriter(uriInfo, jsonOut);

            links.buildApiLinks();
            browseMessages(jsonOut, links, session, queue, query);
        });
        return setModifiedHeaders(Response.ok(streamOut), queue).build();
    }

    private void browseMessages(final JsonOut jsonOut, final LinksWriter links, final QSession session, final Queue q, QBrowseMessageQuery query)
            throws IOException {
        streamQWithLinks(links, jsonOut, q);

        jsonOut.arrayNamed("messages", () -> {
            PageResponse<QMessage> messageStream = session.queueOperations().browser(q).browse(query);
            jsonOut.inObjects(messageStream, msg -> jsonOut.propertyNamed("message", new MessageDTO(msg)));

        });
    }

    private boolean notModified(final Queue queue, final HttpHeaders httpHeaders) {
        Date lastModified = queue.lastModified();
        Option<String> ifModified = option(httpHeaders.getRequestHeaders().getFirst(HttpHeaders.IF_MODIFIED_SINCE));
        if (RestKit.isModifiedSince(ifModified, lastModified)) {
            return true;
        }
        Option<String> noneMatch = option(httpHeaders.getRequestHeaders().getFirst(HttpHeaders.IF_NONE_MATCH));
        return RestKit.ifNoneMatch(noneMatch, lastModified);
    }

    private Response.ResponseBuilder setModifiedHeaders(final Response.ResponseBuilder response, final Queue queue) {
        String lastModified = RestKit.httpDate(queue.lastModified());

        Response.ResponseBuilder out = response.header("Last-Modified", lastModified);
        out = out.header("ETag", lastModified);
        return out;
    }

    public Response getNextMessage(final UriInfo uriInfo, final Long queueId) {
        QSession session = session();
        Queue q = session.queueOperations().getQueue(QId.id(queueId));
        QMessageConsumer consumer = session.createConsumer(q);
        Optional<QMessage> message = consumer.claimAndResolve();
        if (message.isPresent()) {
            StreamingOutput out = JsonOut.run(jsonOut -> {

                LinksWriter links = new LinksWriter(uriInfo, jsonOut);

                links.buildApiLinks();

                streamQWithLinks(links, jsonOut, q);

                jsonOut.toJson("message", new MessageDTO(message.get()));
            });
            return Response.ok(out).build();
        } else {
            return Response.noContent().build();
        }
    }

    public Response putSimpleMessage(UriInfo uriInfo, final Long queueId, final String msg) {
        StreamingOutput out = JsonOut.run(jsonOut -> {
            QMessage simpleMsg = QMessageBuilder.newMsg()
                    .withContentType(QContentType.PLAIN_TEXT)
                    .newBuffer().append(msg).buildBuffer().build();

            QSession session = session();
            Queue queue = session.queueOperations().getQueue(QId.id(queueId));
            QMessageProducer producer = session.createProducer(queue);
            producer.writeMessage(simpleMsg);

            LinksWriter links = new LinksWriter(uriInfo, jsonOut);

            links.buildApiLinks();

            streamQWithLinks(links, jsonOut, queue);

            jsonOut.toJson("message", new MessageNoBufferDTO(simpleMsg));
        });
        return Response.ok(out).build();
    }

    public Response putComplexMessage(UriInfo uriInfo, final Long queueId, final String jsonInput) {
        PutMessageInstruction putMessage = new Gson().fromJson(jsonInput, PutMessageInstruction.class);

        if (putMessage == null) {
            return Response.status(BAD_REQUEST).build();
        }

        StreamingOutput out = JsonOut.run(jsonOut -> {
            QMessage complexMsg = buildMsgFromInstructions(putMessage);

            QSession session = session();
            Queue queue = session.queueOperations().getQueue(QId.id(queueId));
            QMessageProducer producer = session.createProducer(queue);
            producer.writeMessage(complexMsg);

            LinksWriter links = new LinksWriter(uriInfo, jsonOut);

            links.buildApiLinks();

            streamQWithLinks(links, jsonOut, queue);
        });
        return Response.ok(out).build();
    }

    private QMessage buildMsgFromInstructions(final PutMessageInstruction putMessage) {
        final QMessageBuilder builder = QMessageBuilder.newMsg();

        putMessage.getContentType().foreach(contentType -> builder.withContentType(contentType));
        putMessage.getPriority().foreach(priority -> builder.withPriority(priority));
        putMessage.getContentVersion().foreach(contentVersion -> builder.withContentVersion(contentVersion));
        putMessage.getExpiryDate().foreach(expiryDate -> builder.withExpiry(expiryDate));
        putMessage.getContent().foreach(content -> builder.newBuffer().append(content).buildBuffer());

        return builder.build();
    }

    private QSession session() {
        QSessionDefinition sessionDefinition = QSessionDefinitionBuilder.newDefinition()
                .withAutoCommitStrategy()
                .build();
        return qSessionFactory.createSession(sessionDefinition);
    }
}
