package com.atlassian.psmq.api.message;

import com.atlassian.annotations.PublicApi;

import static com.atlassian.psmq.api.internal.Validations.checkNotNull;

/**
 * {@link com.atlassian.psmq.api.message.QMessage}s can have a priority so they can be read in most important order first
 *
 * @since v1.0
 */
@PublicApi
public class QMessagePriority
{
    /**
     * Represents messages that are considered LOW priority
     */
    public static final QMessagePriority LOW = new QMessagePriority(0, Comparison.EQUAL_TO, "LOW");
    /**
     * Represents messages that are considered NORMAL priority
     */
    public static final QMessagePriority NORMAL = new QMessagePriority(4, Comparison.EQUAL_TO, "NORMAL");
    /**
     * Represents messages that are considered HIGH priority
     */
    public static final QMessagePriority HIGH = new QMessagePriority(9, Comparison.EQUAL_TO, "HIGH");


    /**
     * {@link com.atlassian.psmq.api.message.QMessageQuery} can look for messages by priority and this comparison
     * operators are used for that
     */
    public static enum Comparison
    {
        GREATER_THAN,
        GREATER_THAN_OR_EQUAL_TO,
        EQUAL_TO,
        LESS_THAN_OR_EQUAL_TO,
        LESS_THAN
    }

    private final int priority;
    private final Comparison comparison;
    private final String displayName;

    /**
     * A priority with the specified value and a comparison value
     * @param priority the priority of the message
     * @param comparison the comparison operator
     * @param displayName a simple display name
     */
    public QMessagePriority(final int priority, final Comparison comparison, final String displayName)
    {
        this.displayName = displayName;
        this.comparison = checkNotNull(comparison);
        this.priority = priority;
    }

    /**
     * A priority with the specified value and a comparison value of EQUALS_TO
     *
     * @param priority the priority of the message
     */
    public QMessagePriority(final int priority)
    {
        this.priority = priority;
        this.comparison = Comparison.EQUAL_TO;
        this.displayName = String.valueOf(priority);
    }

    /**
     * @return the priority value
     */
    public int value()
    {
        return priority;
    };

    /**
     * @return the priority comparison
     */
    public Comparison comparison()
    {
        return comparison;
    }

    /**
     * @return a simple display name
     */
    public String displayName()
    {
        return displayName;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final QMessagePriority that = (QMessagePriority) o;

        if (priority != that.priority)
        {
            return false;
        }
        if (comparison != that.comparison)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = priority;
        result = 31 * result + comparison.hashCode();
        return result;
    }
}
