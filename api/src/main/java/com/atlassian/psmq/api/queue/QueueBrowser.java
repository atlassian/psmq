package com.atlassian.psmq.api.queue;

import com.atlassian.annotations.PublicApi;
import com.atlassian.psmq.api.message.QMessage;
import com.atlassian.psmq.api.paging.PageResponse;

/**
 * This can read messages from a queue without removing them
 *
 * @since v1.0
 */
@PublicApi
public interface QueueBrowser
{
    /**
     * @return the number of messages in the queue
     */
    long getMessageCount();

    /**
     * This will browse the queue and return the count of messages that match the query criteria
     *
     * @param query the criteria to filter messages on
     * @return the number of messages in the queue that match.  These are not removed from the queue when read
     */
    long getMessageCount(QBrowseMessageQuery query);

    /**
     * This will browse the queue and return messages that match the query criteria
     *
     * @param query the criteria to filter messages on
     * @return a page of messages in the queue.  These are not removed from the queue when read
     */
    PageResponse<QMessage> browse(QBrowseMessageQuery query);
}
